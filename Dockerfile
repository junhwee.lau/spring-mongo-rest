FROM openjdk:12-alpine

RUN apk update && apk add bash

RUN mkdir -p /opt/app
ENV PROJECT_HOME /opt/app

COPY target/demo-0.0.1-SNAPSHOT.jar $PROJECT_HOME/demo-0.0.1-SNAPSHOT.jar

WORKDIR $PROJECT_HOME

ENTRYPOINT ["java","-jar","./demo-0.0.1-SNAPSHOT.jar"]
